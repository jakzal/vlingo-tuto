package be.xl.shop.catalog.event;

import be.xl.shop.catalog.model.ProductId;
import be.xl.shop.catalog.model.Tenant;

public class ProductDescriptionChanged extends ProductEvent {

   public final String description;

   public static ProductDescriptionChanged productDescriptionChanged(final Tenant tenant, final ProductId productId, final String description) {
      return new ProductDescriptionChanged(tenant, productId, description);
   }

   public ProductDescriptionChanged(final Tenant tenant, final ProductId productId, final String description) {
      super(productId.id, tenant.id);
      this.description = description;
   }
}
