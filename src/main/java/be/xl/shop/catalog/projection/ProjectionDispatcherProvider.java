package be.xl.shop.catalog.projection;

import io.vlingo.actors.Definition;
import io.vlingo.actors.Protocols;
import io.vlingo.actors.Stage;
import io.vlingo.lattice.model.projection.ProjectionDispatcher;
import io.vlingo.lattice.model.projection.ProjectionDispatcher.ProjectToDescription;
import io.vlingo.symbio.store.dispatch.Dispatcher;
import io.vlingo.symbio.store.state.StateStore;
import java.util.Arrays;
import java.util.List;

public class ProjectionDispatcherProvider {
   private static ProjectionDispatcherProvider instance;

   public final ProjectionDispatcher projectionDispatcher;
   public final Dispatcher storeDispatcher;

   public static ProjectionDispatcherProvider instance() {
      return instance;
   }

   public static ProjectionDispatcherProvider using(final Stage stage, final StateStore stateStore) {
      if (instance != null) return instance;

      final List<ProjectToDescription> descriptions =
          Arrays.asList(
              ProjectToDescription.with(ProductProjectionActor.class, be.xl.shop.catalog.event.ProductEvent.class.getPackage())
          );

      final Protocols dispatcherProtocols =
          stage.actorFor(
              new Class<?>[] { Dispatcher.class, ProjectionDispatcher.class },
              Definition.has(ProductProjectionActor.class, Definition.parameters(stateStore)));

      final Protocols.Two<Dispatcher, ProjectionDispatcher> dispatchers = Protocols.two(dispatcherProtocols);

      instance = new ProjectionDispatcherProvider(dispatchers._1, dispatchers._2);

      return instance;
   }

   private ProjectionDispatcherProvider(final Dispatcher storeDispatcher, final ProjectionDispatcher projectionDispatcher) {
      this.storeDispatcher = storeDispatcher;
      this.projectionDispatcher = projectionDispatcher;
   }
}
