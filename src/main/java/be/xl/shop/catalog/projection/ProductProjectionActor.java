package be.xl.shop.catalog.projection;

import static be.xl.shop.catalog.projection.ProductView.Builder.aProductView;

import be.xl.shop.catalog.event.ProductDefined;
import be.xl.shop.catalog.event.ProductDescriptionChanged;
import be.xl.shop.catalog.event.ProductEvent;
import be.xl.shop.catalog.event.ProductNameChanged;
import io.vlingo.lattice.model.DomainEvent;
import io.vlingo.lattice.model.projection.Projectable;
import io.vlingo.lattice.model.projection.StateStoreProjectionActor;
import io.vlingo.symbio.Entry;
import io.vlingo.symbio.store.state.StateStore;
import java.util.Collections;
import java.util.List;

public class ProductProjectionActor extends StateStoreProjectionActor<ProductView> {
  private static final ProductView Empty = new ProductView();
  private final List<ProductEvent> events = Collections.emptyList();

  public ProductProjectionActor(StateStore stateStore) {
    super(stateStore);
  }

  @Override
  protected ProductView currentDataFor(Projectable projectable) {
    return Empty;
  }

  @Override
  protected String dataIdFor(Projectable projectable) {
    // projectable.dataId(); ???
    return events.get(0).productId;
  }

  @Override
  protected ProductView merge(
      ProductView previousData, int previousVersion, ProductView currentData, int currentVersion) {
    return mergeInto(currentData);
  }

  @Override
  protected void prepareForMergeWith(Projectable projectable) {
    events.clear();
    projectable.entries().forEach(entry -> events.add(mapToProductEvent(entry)));
  }

  private ProductView mergeInto(final ProductView view) {
    for (final DomainEvent event : events) {
      apply(event, view);
    }
    return view;
  }

  private ProductView apply(DomainEvent event, ProductView view) {
    if (event.getClass().equals(ProductDefined.class)) {
      ProductDefined productDefined = (ProductDefined) event;
      return aProductView(view)
          .productId(productDefined.productId)
          .tenant(productDefined.tenantId)
          .name(productDefined.name)
          .description(productDefined.description)
          .productOwner(productDefined.productOwnerId)
          .build();
    } else if (event.getClass().equals(ProductDescriptionChanged.class)) {
      ProductDescriptionChanged productDescriptionChanged = (ProductDescriptionChanged) event;
      return aProductView(view).description(productDescriptionChanged.description).build();
    } else if (event.getClass().equals(ProductNameChanged.class)) {
      ProductNameChanged productNameChanged = (ProductNameChanged) event;
      return aProductView(view).description(productNameChanged.name).build();
    } else {
      throw new RuntimeException(String.format("Unexpected domain event type %s", event.getClass().getName()));
    }
  }

  private ProductEvent mapToProductEvent(Entry entry) {
    if (entry.typeName().equals(ProductDefined.class.getName())) {
      return ((ProductDefined) entry);
    } else if (entry.typeName().equals(ProductNameChanged.class.getName())) {
      return ((ProductNameChanged) entry);
    } else if (entry.typeName().equals(ProductDescriptionChanged.class.getName())) {
      return ((ProductDescriptionChanged) entry);

    } else {
      throw new RuntimeException(String.format("Unexpected type %s", entry.typeName()));
    }
  }
}
