package be.xl.shop.catalog.projection;

public class ProductView {
  private String tenant;
  private String productOwner;
  private String productId;
  private String name;
  private String description;

  public String getTenant() {
    return tenant;
  }

  public String getProductOwner() {
    return productOwner;
  }

  public String getProductId() {
    return productId;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }


  public static final class Builder {
    private String tenant;
    private String productOwner;
    private String productId;
    private String name;
    private String description;

    private Builder() {
    }

    private Builder(ProductView productView) {
      this.tenant = productView.tenant;
      this.productOwner = productView.productOwner;
      this.productId = productView.productId;
      this.name = productView.name;
      this.description = productView.description;
    }

    public static Builder aProductView() {
      return new Builder();
    }

    public static Builder aProductView(ProductView productView) {
      return new Builder(productView);
    }

    public Builder tenant(String tenant) {
      this.tenant = tenant;
      return this;
    }

    public Builder productOwner(String productOwner) {
      this.productOwner = productOwner;
      return this;
    }

    public Builder productId(String productId) {
      this.productId = productId;
      return this;
    }

    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Builder description(String description) {
      this.description = description;
      return this;
    }

    public ProductView build() {
      ProductView productView = new ProductView();
      productView.name = this.name;
      productView.tenant = this.tenant;
      productView.productId = this.productId;
      productView.productOwner = this.productOwner;
      productView.description = this.description;
      return productView;
    }
  }
}
