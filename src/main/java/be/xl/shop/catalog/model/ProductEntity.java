package be.xl.shop.catalog.model;

import be.xl.shop.catalog.event.ProductDefined;
import be.xl.shop.catalog.event.ProductDescriptionChanged;
import be.xl.shop.catalog.event.ProductNameChanged;
import io.vlingo.common.Completes;
import io.vlingo.lattice.model.sourcing.EventSourced;

public class ProductEntity extends EventSourced implements Product {
  private State state;

  static {
    EventSourced.registerConsumer(
        ProductEntity.class, ProductDefined.class, ProductEntity::applyProductDefined);
    EventSourced.registerConsumer(
        ProductEntity.class, ProductNameChanged.class, ProductEntity::applyProductNameChanged);
    EventSourced.registerConsumer(
        ProductEntity.class,
        ProductDescriptionChanged.class,
        ProductEntity::applyProductDescriptionChanged);
  }

  public ProductEntity(final Tenant tenant, final ProductId productId) {
    super(productId.id);
    logger().info("ProductEntity created");
    System.out.println("ProductEntity created");
    this.state = State.initial(tenant, productId);
  }

  private void applyProductDefined(final ProductDefined e) {
    logger().info("Product defined");
    state =
        new State(
            Tenant.fromExisting(e.tenantId),
            ProductId.fromExisting(e.productId),
            ProductOwner.fromExisting(e.tenantId, e.productOwnerId),
            e.name,
            e.description);
  }

  private void applyProductNameChanged(ProductNameChanged e) {
    logger().info("Product name changed");
    state = state.withName(e.name);
  }

  private void applyProductDescriptionChanged(ProductDescriptionChanged e) {
    logger().info("Product description changed");
    state = state.withDescription(e.description);
  }

  @Override
  public void define(ProductOwner productOwner, String name, String description) {
    apply(
        ProductDefined.productDefined(
            state.tenant, state.productId, productOwner, name, description));
  }

  @Override
  public void changeName(String name) {
    if (!state.name.equals(name)) {
      apply(ProductNameChanged.productNameChanged(state.tenant, state.productId, name));
    }
  }

  @Override
  public Completes<String> changeDescription(String description) {
    if (!state.description.equals(description)) {
      apply(
          ProductDescriptionChanged.productDescriptionChanged(
              state.tenant, state.productId, description),
          () -> description);
    }
    return completes().with(description);
  }

  @Override
  public Completes<State> query() {
    return completes().with(state);
  }

  public static class State {

    public final Tenant tenant;
    public final ProductOwner productOwner;
    public final ProductId productId;
    public final String name;
    public final String description;

    static State initial(final Tenant tenant, final ProductId productId) {
      return new State(tenant, productId, null, null, null);
    }

    static State with(
        final Tenant tenant,
        final ProductId productId,
        final ProductOwner productOwner,
        final String name,
        final String description) {
      return new State(tenant, productId, productOwner, name, description);
    }

    State(
        final Tenant tenant,
        final ProductId productId,
        final ProductOwner productOwner,
        final String name,
        final String description) {
      this.tenant = tenant;
      this.productId = productId;
      this.productOwner = productOwner;
      this.name = name;
      this.description = description;
    }

    State withDescription(final String description) {
      return new State(tenant, productId, productOwner, name, description);
    }

    State withName(final String name) {
      return new State(tenant, productId, productOwner, name, description);
    }

    State withProductOwner(final ProductOwner productOwner) {
      return new State(tenant, productId, productOwner, name, description);
    }
  }
}
