package be.xl.shop.catalog.model;

public class Tenant {
    public final String id;

    public static Tenant fromExisting(final String referencedId) {
        return new Tenant(referencedId);
    }

    public static Tenant with(final String id) {
        return new Tenant(id);
    }

    public Tenant(final String id) {
        assert (id != null);
        this.id = id;
    }
}
