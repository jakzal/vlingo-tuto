package be.xl.shop.catalog.model;

import be.xl.shop.catalog.model.ProductEntity.State;
import io.vlingo.actors.Definition;
import io.vlingo.actors.Stage;
import io.vlingo.common.Completes;
import io.vlingo.common.Tuple2;

public interface Product {
   static Tuple2<ProductId, Product> defineWith(
       final Stage stage,
       final Tenant tenant,
       final ProductOwner productOwner,
       final String name,
       final String description) {

      assert (tenant != null && tenant.id != null);
      assert (productOwner != null && productOwner.id != null);
      assert (name != null);
      assert (description != null);

      final ProductId productId = ProductId.generateNew();
      final Definition definition = Definition.has(ProductEntity.class, Definition.parameters(tenant, productId), productId.id);
      final Product product = stage.actorFor(Product.class, definition, stage.addressFactory().from(productId.id));
      product.define(productOwner, name, description);
      return Tuple2.from(productId, product);
   }

   void define(final ProductOwner productOwner, final String name, final String description);

   void changeName(final String name);

   Completes<String> changeDescription(final String description);

   Completes<State> query();
}
