package be.xl.shop.catalog.infra.dispatch;

import io.vlingo.symbio.Entry;
import io.vlingo.symbio.State;
import io.vlingo.symbio.State.TextState;
import io.vlingo.symbio.store.dispatch.Dispatchable;
import io.vlingo.symbio.store.dispatch.Dispatcher;
import io.vlingo.symbio.store.dispatch.DispatcherControl;

public class NoopEventJournalDispatcher
    implements Dispatcher<Dispatchable<Entry<String>, TextState>> {

  @Override
  public void controlWith(final DispatcherControl control) {}

  @Override
  public void dispatch(final Dispatchable<Entry<String>, State.TextState> dispatchable) {}
}
