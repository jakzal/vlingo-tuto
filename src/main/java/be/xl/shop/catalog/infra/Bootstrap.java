package be.xl.shop.catalog.infra;

import be.xl.shop.catalog.model.ProductEntity;
import be.xl.shop.catalog.infra.dispatch.NoopEventJournalDispatcher;
import be.xl.shop.catalog.infra.resource.ProductResource;
import io.vlingo.actors.World;
import io.vlingo.http.resource.Configuration;
import io.vlingo.http.resource.Resources;
import io.vlingo.http.resource.Server;
import io.vlingo.lattice.model.sourcing.SourcedTypeRegistry;
import io.vlingo.symbio.store.DataFormat;
import io.vlingo.symbio.store.common.jdbc.DatabaseType;
import io.vlingo.symbio.store.common.jdbc.postgres.PostgresConfigurationProvider;
import io.vlingo.symbio.store.dispatch.NoOpDispatcher;
import io.vlingo.symbio.store.journal.Journal;
import io.vlingo.symbio.store.journal.inmemory.InMemoryJournalActor;
import io.vlingo.symbio.store.journal.jdbc.JDBCJournalActor;
import org.flywaydb.core.Flyway;

public class Bootstrap {
    private static Bootstrap instance;
    private final World world;
    private final Server server;

    private static final String DB_URL = "jdbc:postgresql://[::1]:5432/";
    private static final String DB_USER = "vlingo_test";
    private static final String DB_PWD = "vlingo123";
    private static final String DB_NAME = "vlingo_test";

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Bootstrap() {

        world = World.startWithDefaults("product");

        //Journal journal = buildInMemoryJournal();
        Journal journal = buildJdbcJournal();

        SourcedTypeRegistry registry = new SourcedTypeRegistry(world);
        final SourcedTypeRegistry.Info info = new SourcedTypeRegistry.Info(journal, ProductEntity.class, ProductEntity.class.getSimpleName());
        registry.register(info);

        final ProductResource productResource = new ProductResource(world);
        final Resources resources = Resources.are(productResource.routes());

        this.server = Server.startWith(world.stage(),
                resources,
                8080,
                Configuration.Sizing.define(),
                Configuration.Timing.define());

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (instance != null) {
                instance.server.stop();

                System.out.println("\n");
                System.out.println("=======================");
                System.out.println("Stopping product-service.");
                System.out.println("=======================");
                pause();
            }
        }));
    }

    private Journal buildInMemoryJournal() {
        NoOpDispatcher noOpDispatcher = new NoOpDispatcher();
        return world.actorFor(Journal.class, InMemoryJournalActor.class, noOpDispatcher);
    }

    private Journal buildJdbcJournal() {
        Flyway.configure().dataSource(DB_URL, DB_USER, DB_PWD).load().migrate();
        final io.vlingo.symbio.store.common.jdbc.Configuration configuration;
        try {
            configuration =
                new io.vlingo.symbio.store.common.jdbc.Configuration(
                    DatabaseType.Postgres,
                    PostgresConfigurationProvider.interest,
                    "org.postgresql.Driver",
                    DataFormat.Text,
                    DB_URL,
                    DB_NAME,
                    DB_USER,
                    DB_PWD,
                    false,
                    "",
                    false);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        final NoopEventJournalDispatcher journalDispatcher = new NoopEventJournalDispatcher();
        return Journal.using(world.stage(), JDBCJournalActor.class, journalDispatcher, configuration);
    }

    public static void main(String[] args) throws Exception {
        System.out.println("=======================");
        System.out.println("service: product-service.");
        System.out.println("=======================");
        Bootstrap.instance();
    }

    static Bootstrap instance() throws Exception {
        if (instance == null) {
            instance = new Bootstrap();
        }
        return instance;
    }

    private void pause() {
        try {
            Thread.sleep(1000L);
        } catch (Exception e) {
            // ignore
        }
    }
}
